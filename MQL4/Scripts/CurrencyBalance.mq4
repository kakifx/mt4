//+------------------------------------------------------------------+
//|                                              CurrencyBalance.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {

//---
   string currencies[8];
   currencies[0] = "EUR";
   currencies[1] = "GBP";
   currencies[2] = "AUD";
   currencies[3] = "NZD";
   currencies[4] = "USD";
   currencies[5] = "CAD";
   currencies[6] = "CHF";
   currencies[7] = "JPY";

   int n=ArraySize(currencies);

   for(int i=0; i<n; i++)
     {
      for(int j=i+1; j<n; j++)
        {
         for(int k=0; k<n; k++)
           {
            if(k==j) continue;

            string basePair=StringConcatenate(currencies[i],currencies[j]);
            double basePrice=(MarketInfo(basePair,MODE_ASK)+MarketInfo(basePair,MODE_BID))/2;
            string compair0;
            string compair1;

            if(k>i && k>j)
              {
               compair0 = StringConcatenate(currencies[i], currencies[k]);
               compair1 = StringConcatenate(currencies[j], currencies[k]);
               double compair0Price = (MarketInfo(compair0, MODE_ASK) + MarketInfo(compair0, MODE_BID)) / 2;
               double compair1Price = (MarketInfo(compair1, MODE_ASK) + MarketInfo(compair1, MODE_BID)) / 2;
               double value=(compair0Price/compair1Price)/basePrice;
               Print("(",compair0," / ",compair1,") / ",basePair," = ",value);
              }
            else if(k>i && k<j)
              {
               compair0 = StringConcatenate(currencies[i], currencies[k]);
               compair1 = StringConcatenate(currencies[k], currencies[j]);
               double compair0Price = (MarketInfo(compair0, MODE_ASK) + MarketInfo(compair0, MODE_BID)) / 2;
               double compair1Price = (MarketInfo(compair1, MODE_ASK) + MarketInfo(compair1, MODE_BID)) / 2;
               double value=(compair0Price*compair1Price)/basePrice;
               Print("(",compair0," * ",compair1,") / ",basePair," = ",value);
              }
            else if(k<i && k<j)
              {
               compair0 = StringConcatenate(currencies[k], currencies[i]);
               compair1 = StringConcatenate(currencies[k], currencies[j]);
               double compair0Price = (MarketInfo(compair0, MODE_ASK) + MarketInfo(compair0, MODE_BID)) / 2;
               double compair1Price = (MarketInfo(compair1, MODE_ASK) + MarketInfo(compair1, MODE_BID)) / 2;
               double value=(compair1Price/compair0Price)/basePrice;
               Print("(",compair1," / ",compair0,") / ",basePair," = ",value);
              }
           }
         Print("----------");
        }
     }
  }
//+------------------------------------------------------------------+
